﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using cfpt_quadtreee.Tools;
using QuadTrees;

namespace cfpt_quadtreee
{
    public partial class frmMain : Form
    {
        private Random _random = new(1234);
        private QuadTreeRect<Cube> _quadtree;
        private List<Cube> _cubes;

        public const int W = 1500;
        public const int H = 1200;
        public const int S = 4;
        public const int NumberExec = 3;
        public const int NumberCube = 10000;

        private string[] _show = new string[5];

        private List<double>[] _executions = new List<double>[5];
        
        private bool _useQuadTree = true;
        
        public frmMain()
        {
            InitializeComponent();
            
            DoubleBuffered = true;
            
            Paint += OnPaint;

            long start = DateTime.Now.Ticks;
            
            _cubes = new List<Cube>();
            if (_useQuadTree)
               _quadtree = new QuadTreeRect<Cube>();

            for (int i = 0; i < NumberCube; i++)
            {
                Cube p = new Cube(
                    new Point(_random.Next(W), _random.Next(H)),
                    new Size(S,S),
                    (Direction) _random.Next(8), _random.Next(S/2,S)
                );
                _cubes.Add(p);
            }
            if (_useQuadTree)
                UpdateQuad();
            
            long end = DateTime.Now.Ticks;

            AddPerf(start, end, "frmMain : {0}ms", 0);
            
            Timer timer = new Timer {Interval = 1000 / 20, Enabled = true};
            timer.Tick += TimerOnTick;
        }

        private void AddPerf(long start, long end, string msg, int index)
        {
            long interval = end - start;
            double ms = (double)interval / TimeSpan.TicksPerMillisecond;

            _executions[index] ??= new List<double>();
            
            _executions[index].Add(ms);
            if (_executions[index].Count > NumberExec) _executions[index].RemoveAt(0);

            double msTot = _executions[index].Sum() / _executions[index].Count;
            
            _show[index] = string.Format(msg + " (Current : {1}ms)", $"{msTot:F}", $"{ms:F}");
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            long start = DateTime.Now.Ticks;
            
            /*if (_useQuadTree) 
                DrawQuadTree(_quadtree, e.Graphics);*/
            DrawRectangles(e.Graphics);

            long end = DateTime.Now.Ticks;

            AddPerf(start, end, "OnPaint : {0}ms", 2);
        }

        private void UpdateQuad()
        {
            long start = DateTime.Now.Ticks;
            
            _quadtree.Clear();
            foreach (Cube cube in _cubes) _quadtree.Add(cube);

            long end = DateTime.Now.Ticks;

            AddPerf(start, end, "\r\nUpdateQuad : {0}ms", 1);
        }

        private void DrawQuadTree(QuadTreeRect<Cube> quad, Graphics g)
        {
            /*if (quad.CountNodes == 0) {
                g.DrawRectangle(Pens.Black, Rectangle.Round(new RectangleF(bounds.X, bounds.Y, bounds.Width, bounds.Height)));
            } else {
                DrawQuadTree(quad.,g);
                DrawQuadTree(quad.,g);
                DrawQuadTree(quad.,g);
                DrawQuadTree(quad.,g);
            }*/
        }

        private void DrawRectangles(Graphics g)
        {
            foreach (Cube cube in _cubes)
            {
                g.FillRectangle(Brushes.Aqua, new Rectangle(cube.Location, new Size(S,S)));
            }
            g.DrawRectangle(Pens.Brown, new Rectangle(0, 0, W, H));
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            long start = DateTime.Now.Ticks;
            
            foreach (Cube cube in _cubes)
            {
                if (cube.Location.X > W - S ||
                    cube.Location.X < S ||
                    cube.Location.Y > H - S ||
                    cube.Location.Y < S)
                    cube.Direction = Utils.InvertDirection(cube.Direction);
                
                Point newLoc = Utils.AddToPoint(cube.Location, Utils.DirectionToPoint(cube.Velocity, cube.Direction));
                
                cube.Location = newLoc;
            }

            List<Cube> cassercube = new(_cubes);
            List<Cube> newCubes = new();
            
            if (_useQuadTree)
            {
                using List<Cube>.Enumerator v = _cubes.GetEnumerator();
                while(v.MoveNext())
                {
                    Cube cube = v.Current;
                    foreach (Cube cubeCollided in _quadtree.GetObjects(new Rectangle(cube.Location.X, cube.Location.Y, S, S)).Where(c => c != cube))
                    {
                        cubeCollided.Location = cubeCollided.OldLocation;
                        cubeCollided.Direction = Utils.InvertDirection(cubeCollided.Direction);
                    }
                }
            }
            else
            {
                Parallel.ForEach(_cubes, cube1 =>
                {
                    Parallel.ForEach(cassercube, cube2 =>
                    {
                        if (cube1.Equals(cube2))
                        {
                            newCubes.Add(cube2);
                        }

                    });

                });

                foreach (Cube cube1 in newCubes)
                {
                    cube1.Location = cube1.OldLocation;
                    cube1.Direction = Utils.InvertDirection(cube1.Direction);
                }
            }
            /*else
            {
                /*while (cassercube.Count > 0)
                {
                    Cube c = cassercube[0];
                    cassercube.RemoveAt(0);
                    foreach (var cb in cassercube)
                    {
                        if (IntersectWith(c, cb))
                        {
                            cb.Location = cb.OldLocation;
                            cb.Direction = Utils.InvertDirection(cb.Direction);
                        }
                    }
                }
                
            }
        }

/*                    
                foreach (Cube cube1 in _cubes)
                {
                    foreach (Cube cube2 in cassercube)  
                    {
                        if (cube1.Equals(cube2))
                        {
                            cube2.Location = cube2.OldLocation;
                            cube2.Direction = Utils.InvertDirection(cube2.Direction);
                        }
                    }
                }
*/                    
            /*foreach (Cube cubeCollided in _cubes.Where(c=> cube.Rect.IntersectsWith(c.Rect)).Where(c => c != cube))
               {
                   cubeCollided.Location = cubeCollided.OldLocation;
                   cubeCollided.Direction = Utils.InvertDirection(cubeCollided.Direction);
               }*/
               
            if(_useQuadTree)
                UpdateQuad();
            Invalidate();

            long end = DateTime.Now.Ticks;

            AddPerf(start, end, "TimerOnTick : {0}ms", 3);
            
            
            double ms = 0;
            
            for (int i = 1; i < _executions.Length; i++)
            {
                if (_executions[i] == null || _executions[i].Count == 0) continue;
                ms += _executions[i][_executions[i].Count - 1];
            }

            lblInfo.Text = String.Join("\r\n", _show) + $"\r\nTotal MS : {ms:F}ms\r\nTPS : {(50.0 / (ms < 50 ? 50 : ms) * 20):F}/20 TPS";
        }
    }
}