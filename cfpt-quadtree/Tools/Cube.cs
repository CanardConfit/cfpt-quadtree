﻿using System;
using System.Drawing;
using QuadTrees.QTreeRect;

namespace cfpt_quadtreee.Tools
{
    public class Cube : IRectQuadStorable, IEquatable<Cube>
    {
        private Point _location;
        private Size _size;
        private Point _oldLocation;
        private Direction _direction;
        private int _velocity;
        
        public Point Location
        {
            get => _location;
            set
            {
                _oldLocation = _location;
                _location = value;
            }
        }

        public Direction Direction
        {
            get => _direction;
            set => _direction = value;
        }

        public int Velocity
        {
            get => _velocity;
            set => _velocity = value;
        }

        public Point OldLocation
        {
            get => _oldLocation;
        }

        public Cube(Point location, Size size, Direction direction, int velocity)
        {
            _location = location;
            _size = size;
            _direction = direction;
            _velocity = velocity;
        }

        public Rectangle Rect => new (_location, SizeCube);

        public Size SizeCube
        {
            get => _size;
            set => _size = value;
        }

        public bool Equals(Cube? other)
        {
            return this != other && this.Rect.IntersectsWith(other.Rect);
        }
    }
}