﻿namespace cfpt_quadtreee.Tools
{
    public enum Direction
    {
        Top       = 0,
        TopRight  = 1,
        Right     = 2,
        RightBack = 3,
        Back      = 4,
        BackLeft  = 5,
        Left      = 6,
        LeftTop   = 7
    }
}