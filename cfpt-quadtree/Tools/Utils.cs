﻿using System;
using System.Drawing;

namespace cfpt_quadtreee.Tools
{
    public static class Utils
    {
        public static Point DirectionToPoint(int velocity, Direction direction)
        {
            Point point = direction switch
            {
                Direction.Top => new Point(0, -velocity),
                Direction.TopRight => new Point(velocity, -velocity),
                Direction.Right => new Point(velocity, 0),
                Direction.RightBack => new Point(velocity, velocity),
                Direction.Back => new Point(0, velocity),
                Direction.BackLeft => new Point(-velocity, velocity),
                Direction.Left => new Point(-velocity, 0),
                Direction.LeftTop => new Point(-velocity, -velocity),
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
            };

            return point;
        }

        public static Direction InvertDirection(Direction direction)
        {
            if (direction < (Direction) 4)
                return direction + 4;
            return direction - 4;
        }

        public static Point AddToPoint(Point point1, Point point2)
        {
            return new(point1.X + point2.X, point1.Y + point2.Y);
        }
    }
}